import axios from 'axios'
import fs from 'fs'

const numPagParaExtracao = 10
const API_KEY = ''


var data = {
    movies: []
}

var popularIds = []

for (var i = 1; i <= numPagParaExtracao; i++) {
    var response = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=pt-BR&page=${i}`)
        .then(res => {
            console.log(`PAGINA ${i} PROCESSADA, INDO PARA A ${i + 1}`)
            res.data.results.forEach(movie => {
                popularIds.push(movie.id)
                console.log(`ID ${movie.id} EXTRAIDO`);
            })
            console.log('COMEÇANDO REQUISIÇÃO POR ID');
        })
        .catch((err) => console.log(err))
}

popularIds.forEach(id => {
    console.log(id)
    axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}&language=pt-BR&append_to_response=credits,videos`)
        .then(res => {
            const temp = {
                ...res.data,
                poster_path: `https://image.tmdb.org/t/p/original/${res.data.poster_path}`,
                backdrop_path: `https://image.tmdb.org/t/p/original/${res.data.backdrop_path}`
            }
            data.movies.push(temp)
            console.log(`FILME ${res.data.original_title} EXTRAIDO`)
        })

        .catch(err => {
            console.log(`Filme ID ${i} NÃO EXISTE`);
        })
        .then(() => {

            // popularIds = JSON.stringify(popularIds);
            // fs.writeFile('popularIds.txt', popularIds, 'utf8', () => { });

            var json = JSON.stringify(data)
            fs.writeFile('db.json', json, 'utf8', () => { console.log(`FIM DA EXTRAÇÃO, ${popularIds.length} FILMES EXTRAIDOS`) });

        })
})


